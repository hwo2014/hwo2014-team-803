package main

import (
	"fmt"
	"net"
	"os"
	"encoding/json"
	"time"
	"bufio"
	"noobDomain"
	"math"
	"strings"
)

var running bool
var buf = make([]byte, 4048)
var botname = string(os.Args[3])
var throttle float64 = 0.6
var reader *bufio.Reader
var lastAngle float64 = 0.0
var raceStart noobDomain.MessageType
var raceInit, track, join, raceTrackName string
var pieces []noobDomain.Pieces
var lanes []noobDomain.Lanes
var currentPiece int = 0
var currentLane int
var minThrottleAmountOnTrack, realMinThrottleAmountOnTrack float64
var laneSwitchAsked = false

func main() {
	running = true;
	host := string(os.Args[1])
	port := string(os.Args[2])
	botkey := string(os.Args[4])
	if len(os.Args) > 5 {
		track = string(os.Args[5])
		join = string(os.Args[6])
	}
	remote := host + ":" + port

	if track != "" && join != "join" {
		raceInit = createRace(botname, botkey, track, 1, false)
	} else if(track != "" && join == "join") {
		raceInit = createRace(botname, botkey, track, 2, true)
	} else {
		raceInit = joinMessage(botname, botkey)
	}

	if botname == "" {
		botname = "Tryout"
	}

	con, error := net.Dial("tcp", remote)
	defer con.Close()
	if error != nil {
		fmt.Printf("Host not found: %s\n", error)
		con.Close()
		os.Exit(1)
	}
	reader = bufio.NewReader(con);

	go messageHandler(con, raceInit)
	for ;running; {
		time.Sleep(1*1e9)
	}
}

func messageHandler(con net.Conn, joinMsg string) {
	con.Write([]byte(joinMsg + "\n"));
	if b, err := con.Read(buf); err == nil {
		fmt.Println("joined " + string(buf[0:b]))
	}
	for running {
		var messageType noobDomain.MessageType
		var carPositions noobDomain.CarPositions
		var message = Read(con)

		err := json.Unmarshal(message, &messageType)
		if err != nil {
			if messageType.MsgType == "carPositions" {
				json.Unmarshal(message, &carPositions)
			} else {
				fmt.Println("error2:", messageType.MsgType, messageType)
			}
		}

		if(messageType.MsgType == "join") {
			fmt.Println("Joined")
			WritePing(con, marshalPing())
		} else if(messageType.MsgType == "crash") {
			throttle = 0
			fmt.Println("Crashed at")
			WritePing(con, marshalPing())
		} else if(messageType.MsgType == "spawn") {
			throttle = 0.4
			fmt.Println("Spawned")
			WritePing(con, marshalPing())
		} else if(messageType.MsgType == "gameInit") {
			raceTrackName = strings.ToLower(messageType.Data.Race.Track.Name)
			if(raceTrackName == "usa") {
				minThrottleAmountOnTrack = 0.5
				realMinThrottleAmountOnTrack = 0.5
			} else if(raceTrackName == "france") {
				minThrottleAmountOnTrack = 0.5
				realMinThrottleAmountOnTrack = 0.5
			} else if(raceTrackName == "germany") {
				minThrottleAmountOnTrack = 0.4
				realMinThrottleAmountOnTrack = 0.4
			} else if(raceTrackName == "keimola") {
				minThrottleAmountOnTrack = 0.5
				realMinThrottleAmountOnTrack = 0.5
			}  else {
				minThrottleAmountOnTrack = 0.3
				realMinThrottleAmountOnTrack = 0.3
			}
			fmt.Println("Race init", "Laps", messageType.Data.Race.RaceSession.Laps, len(messageType.Data.Race.Track.Pieces))
			pieces = messageType.Data.Race.Track.Pieces
			lanes = messageType.Data.Race.Track.Lanes
			WritePing(con, marshalPing())
		} else if(messageType.MsgType == "gameEnd") {
			fmt.Println("Race end")
			WritePing(con, marshalPing())
		} else if(messageType.MsgType == "gameStart") {
			fmt.Println("Race start")
			raceStart = messageType
			WritePing(con, marshalPing())
		} else if(messageType.MsgType == "lapFinished") {
			fmt.Println("Lap finished, time", messageType.Data.LapTime)
			WritePing(con, marshalPing())
		} else if(carPositions.MsgType == "carPositions") {
			currentAngle := Round(currentAngle(carPositions), 3)
			currentPosition := currentPosition(carPositions)
			currentPieceLength := pieces[currentPosition].Length
			carPositionOnPiece := currentPositionOnPiece(carPositions)
			var nextPosition = currentPosition + 1
			var nextNextPosition = currentPosition + 2
			if nextPosition > len(pieces) - 1 {
				nextPosition = 0
			}
			if nextNextPosition > len(pieces) - 1 {
				nextNextPosition = 0
			}
			currentLane = getCurrentLane(carPositions)

			if (throttle == realMinThrottleAmountOnTrack &&
				((currentAngle > 0 && currentAngle < 1) || (currentAngle < 0 && currentAngle > -1)) &&
				currentPieceLength/carPositionOnPiece < 3 && throttle <= 0.7 &&
				minThrottleAmountOnTrack < realMinThrottleAmountOnTrack + 0.2) {
				minThrottleAmountOnTrack += 0.2
			} else if((currentAngle > 8 || currentAngle < -8) && minThrottleAmountOnTrack == realMinThrottleAmountOnTrack && throttle > 0.1) {
				minThrottleAmountOnTrack -= 0.1
			} else if((currentAngle > 12 || currentAngle < -12) && minThrottleAmountOnTrack == realMinThrottleAmountOnTrack - 0.1 && throttle > 0.1) {
				minThrottleAmountOnTrack -= 0.1
			} else if((currentAngle < 8 || currentAngle > -8)) {
				minThrottleAmountOnTrack = realMinThrottleAmountOnTrack
			}

			fmt.Print("ang: ", pieces[currentPosition].Angle,
				" curPiece: ", currentPosition,
				" nextPieceAng ", pieces[nextPosition].Angle,
				" pieceRad ", pieces[currentPosition].Radius,
				" nextPieceRad ", pieces[nextPosition].Radius,
				" ", raceTrackName,
				" ", currentLane,
				" minThrottle ", realMinThrottleAmountOnTrack, Round(minThrottleAmountOnTrack, 4),
				" tick ", carPositions.GameTick,
				" ")

			if(carPositions.GameTick > 2 && laneSwitchAsked == false && switchLane(carPositionOnPiece, currentPosition, carPositions)) {
				if(len(lanes) > currentLane + 1 && len(lanes) > 1) {
					Write(con, marshalLaneChange("Right"), currentAngle, lastAngle)
					laneSwitchAsked = true
				} else if(currentLane - 1 >= 0) {
					Write(con, marshalLaneChange("Left"), currentAngle, lastAngle)
					laneSwitchAsked = true
				} else {
					Write(con, marshalPing(), currentAngle, lastAngle)
				}
			} else if(shouldIncreaseSpeed(currentAngle, pieces[nextPosition].Angle, pieces[currentPosition].Angle, pieces[nextNextPosition].Angle, pieces[currentPosition].Radius, pieces[nextPosition].Radius)) {
				fmt.Print(" increasing ")
				if ((currentAngle > 0.0 && (currentAngle < lastAngle + 2) || currentAngle < 0.0 && (currentAngle > lastAngle - 2)) &&
					((throttle + 0.3) < 1.00) &&
					!nextPieceAngleDiffers20(nextPosition, nextNextPosition) &&
					(((pieces[nextNextPosition].Angle > 0 && pieces[nextNextPosition].Angle < 45) || (pieces[nextNextPosition].Angle < 0 && pieces[nextNextPosition].Angle > -45)) && pieces[nextNextPosition].Radius < 100) &&
					currentAngle < 20) {
					fmt.Print(" add ")
					setThrottle(con, throttle+0.2, currentAngle, lastAngle)
				} else if(currentPieceLength/carPositionOnPiece < 6 && (pieces[nextNextPosition].Angle <= -45 || pieces[nextNextPosition].Angle >= 45)) {
					if(throttle - 0.05 >= minThrottleAmountOnTrack && throttle - 0.2 > 0) {
						fmt.Print(" remove ")
						setThrottle(con, throttle-0.2, currentAngle, lastAngle)
					} else {
						Write(con, marshalPing(), currentAngle, lastAngle)
					}
				} else if ((throttle + 0.1) <= 1.00 && !nextPieceAngleDiffers20(nextPosition, nextNextPosition)) {
					setThrottle(con, throttle+0.1, currentAngle, lastAngle)
				} else {
					if(raceTrackName == "usa") {
						setThrottle(con, throttle+0.03, currentAngle, lastAngle)
					} else if(!nextPieceAngleDiffers45(nextPosition, nextNextPosition)) {
						setThrottle(con, throttle+0.05, currentAngle, lastAngle)
					} else {
						Write(con, marshalPing(), currentAngle, lastAngle)
					}
				}
			} else if(shouldDecreaseSpeed(currentAngle, pieces[nextPosition].Angle, pieces[currentPosition].Angle, pieces[nextNextPosition].Angle, pieces[currentPosition].Radius, pieces[nextPosition].Radius)) {
				fmt.Print(" decreasing ")
				if(throttle == 1.0) {
					fmt.Print(" a ")
					if(raceTrackName == "usa") {
						if(currentAngle < 10) {
							Write(con, marshalPing(), currentAngle, lastAngle)
						} else {
							setThrottle(con, throttle-0.03, currentAngle, lastAngle)
						}
					} else {
						if (((pieces[nextPosition].Angle > 22 && pieces[nextPosition].Angle < 45 || pieces[nextPosition].Angle < -22 && pieces[nextPosition].Angle < -45) && pieces[nextPosition].Radius >= 100) ||
							((pieces[nextPosition].Angle >= 45 || pieces[nextPosition].Angle <= -45) && pieces[nextPosition].Radius >= 50) ||
							((pieces[nextNextPosition].Angle >= 45 || pieces[nextNextPosition].Angle <= -45) && pieces[nextNextPosition].Radius >= 50)) && throttle > 0.7 {
							setThrottle(con, throttle-0.2, currentAngle, lastAngle)
						} else {
							Write(con, marshalPing(), currentAngle, lastAngle)
						}
					}
				} else if((((pieces[currentPosition].Angle >= 45 || pieces[currentPosition].Angle <= -45) && (pieces[nextPosition].Angle <= -45||pieces[nextPosition].Angle >= 45)) ||
						((pieces[currentPosition].Angle >= 22 || pieces[currentPosition].Angle <= -22) && pieces[nextPosition].Radius >= 100 ) ||
						((pieces[nextPosition].Angle >= 45 || pieces[nextPosition].Angle <= -45) && pieces[nextPosition].Radius >= 50)) &&
						(throttle - 0.1 >= minThrottleAmountOnTrack)) {
					fmt.Print(" b ")
					setThrottle(con, throttle-0.1, currentAngle, lastAngle)
				} else if((pieces[nextPosition].Angle <= -45||pieces[nextPosition].Angle >= 45) &&
						(pieces[nextNextPosition].Angle <= -45||pieces[nextNextPosition].Angle >= 45) &&
						(throttle - 0.1 >= minThrottleAmountOnTrack)) {
					fmt.Print(" c ")
					if (currentPieceLength/carPositionOnPiece < 8 && throttle >= 0.2) {
						if throttle >= 0.5 {
							throttle -= 0.1
						}
						setThrottle(con, throttle-0.1, currentAngle, lastAngle)
					} else {
						Write(con, marshalPing(), currentAngle, lastAngle)
					}
				} else if((pieces[currentPosition].Angle >= 45 || pieces[currentPosition].Angle <= -45) && (currentAngle > 5 || currentAngle < -5) && throttle >= minThrottleAmountOnTrack - 0.1) {
					setThrottle(con, throttle-0.1, currentAngle, lastAngle)
				} else {
					Write(con, marshalPing(), currentAngle, lastAngle)
				}
			} else if(currentAngle == 0.0 && throttle < 1.0 && !nextPieceAngleDiffers20(nextPosition, nextNextPosition)) {
				setThrottle(con, 1.0, currentAngle, lastAngle)
			} else {
				Write(con, marshalPing(), currentAngle, lastAngle)
			}
			lastAngle = currentAngle
		} else {
			WritePing(con, marshalPing())
		}
	}
}

func nextPieceAngleDiffers20(nextPosition int, nextNextPosition int) bool {
	if (pieces[nextPosition].Angle > 20 || pieces[nextPosition].Angle < -20) ||
		(pieces[nextNextPosition].Angle > 40 || pieces[nextNextPosition].Angle < -40) {
		return true
	}
	return false
}

func nextPieceAngleDiffers45(nextPosition int, nextNextPosition int) bool {
	if (pieces[nextPosition].Angle > 40 || pieces[nextPosition].Angle < -40) {
		return true
	}
	return false
}

func shouldIncreaseSpeed(currentAngle float64,
	nextPieceAngle float64,
	currentPieceAngle float64,
	nextNextPositionAngle float64,
	currentPositionRadius float32,
	nextPositionRadius float32) bool {
	if(shouldIncreaseSpeedAngleAtAngle45(currentAngle, nextPieceAngle, currentPieceAngle, nextNextPositionAngle, currentPositionRadius, nextPositionRadius) ||
			shouldIncreaseSpeedAngleAtAngle25(currentAngle, nextPieceAngle, currentPieceAngle, nextNextPositionAngle, currentPositionRadius, nextPositionRadius) ||
			shouldIncreaseSpeedAngleAtAngleZero(currentAngle, nextPieceAngle, currentPieceAngle, nextNextPositionAngle, currentPositionRadius, nextPositionRadius)) {
		return true
	}
	return false
}

func shouldIncreaseSpeedAngleAtAngle45(currentAngle float64,
	nextPieceAngle float64,
	currentPieceAngle float64,
	nextNextPositionAngle float64,
	currentPositionRadius float32,
	nextPositionRadius float32) bool {
		return ((currentPieceAngle >= 25 && currentAngle <= lastAngle && currentPieceAngle <= 45) ||
			(currentPieceAngle <= -25 && currentAngle >= lastAngle && currentPieceAngle >= -45) && currentPositionRadius < 100) &&
			(((nextPieceAngle >= 0 && nextPieceAngle <= 45) || (nextPieceAngle <= 0 && nextPieceAngle >= -45)) &&
			nextPositionRadius < 100) &&
			(currentAngle <= 0 && currentAngle >= 10 || currentAngle >= 0 && currentAngle <= 10) &&
			throttle <= minThrottleAmountOnTrack
}
func shouldIncreaseSpeedAngleAtAngle25(currentAngle float64,
	nextPieceAngle float64,
	currentPieceAngle float64,
	nextNextPositionAngle float64,
	currentPositionRadius float32,
	nextPositionRadius float32) bool {
		return ((currentPieceAngle > 0 && currentAngle < 5 && currentPieceAngle < 25) ||
			(currentPieceAngle < 0 && currentAngle > -5 && currentPieceAngle > -25) && currentPositionRadius < 100) &&
			(((nextPieceAngle > 44 || nextPieceAngle < -44) && nextPositionRadius <= 50) ||
			(((nextPieceAngle > 20 && nextPieceAngle < 25) || (nextPieceAngle < -20 && nextPieceAngle > -25)) && nextPositionRadius <= 100) ||
			((nextNextPositionAngle > 44 || nextNextPositionAngle < -44) && nextPositionRadius < 100)) &&
			throttle <= 0.6
}

func shouldIncreaseSpeedAngleAtAngleZero(currentAngle float64,
	nextPieceAngle float64,
	currentPieceAngle float64,
	nextNextPositionAngle float64,
	currentPositionRadius float32,
	nextPositionRadius float32) bool {
		return (currentPieceAngle > -25 && currentPieceAngle < 25 && currentAngle < 20 && currentAngle > -20 && currentPositionRadius < 150) &&
			((((nextPieceAngle > 44 || nextPieceAngle < -44) && nextPositionRadius < 50) ||
			(((nextPieceAngle > 20 && nextPieceAngle < 25) || (nextPieceAngle < -20 && nextPieceAngle > -25)) && nextPositionRadius < 100)||
			((nextNextPositionAngle > 44 || nextNextPositionAngle < -44) && nextPositionRadius < 100)) ||
			(nextPieceAngle > -0.5 && nextPieceAngle < 0.5)) && throttle < 1.0
}
func shouldDecreaseSpeed(currentAngle float64,
	nextPieceAngle float64,
	currentPositionAngle float64,
	nextNextPositionAngle float64,
	currentPositionRadius float32,
	nextPositionRadius float32) bool {

	return (((currentAngle > 0.0 && currentAngle > lastAngle + 3) || (currentAngle < 0.0 && currentAngle < lastAngle + 3)) ||
			(currentAngle > 25 || currentAngle < -25) ||
			(((nextPieceAngle > 22 || nextPieceAngle < 25) || (nextPieceAngle < -22 || nextPieceAngle > -25)) && nextPositionRadius >= 100) ||
			((nextPieceAngle > 25 || nextPieceAngle < -25) && nextPositionRadius >= 50) ||
			((nextNextPositionAngle > 25 || nextNextPositionAngle < -25))) ||
			throttle > minThrottleAmountOnTrack
}

func setThrottle(con net.Conn, newThrottle float64, currentAngle float64, lastAngle float64) {
	throttle = Round(newThrottle, 4)
	Write(con, marshalThrottle(), currentAngle, lastAngle)
}

func Read(con net.Conn) []byte {
	input, err := reader.ReadString('\n');
	if err!=nil {
		fmt.Println("Error: ", err)
		con.Close()
		running=false
		os.Exit(2)
	}
	//fmt.Println("server message:", input)
	return([]byte(input))
}

func Write(con net.Conn, message string, currentAngle float64, lastAngle float64) {
	con.Write([]byte(message + "\n"))
	fmt.Println("msg:", message, "throttle", throttle, "angle", currentAngle, "prevAngle", lastAngle)
}

func WritePing(con net.Conn, message string) {
	con.Write([]byte(message + "\n"))
	fmt.Println("wrote:", message)
}

func currentAngle(carPositions noobDomain.CarPositions) float64 {
	for _, data := range carPositions.CarPositionData {
		if data.Id.Name == botname {
			return data.Angle
		}
	}
	return 0
}
func currentPosition(carPositions noobDomain.CarPositions) int {
	for _, data := range carPositions.CarPositionData {
		if data.Id.Name == botname {
			if data.PiecePosition.Lane.StartLaneIndex != data.PiecePosition.Lane.EndLaneIndex {
				laneSwitchAsked = false
			}
			return data.PiecePosition.PieceIndex
		}
	}
	return 0
}

func currentPositionOnPiece(carPositions noobDomain.CarPositions) float32 {
	for _, data := range carPositions.CarPositionData {
		if data.Id.Name == botname {
			return data.PiecePosition.InPieceDistance
		}
	}
	return 0
}

func getCurrentLane(carPositions noobDomain.CarPositions) int {
	for _, data := range carPositions.CarPositionData {
		if data.Id.Name == botname {
			return data.PiecePosition.Lane.StartLaneIndex
		}
	}
	return 0
}

func switchLane(carPositionOnPiece float32, currentPosition int, carPositions noobDomain.CarPositions) bool {
	if(len(lanes) > currentLane + 1 && len(lanes) > 1) {
		return true
	}
	for _, data := range carPositions.CarPositionData {
		if (data.Id.Name != botname &&
			data.PiecePosition.PieceIndex == currentPosition &&
			data.PiecePosition.InPieceDistance > carPositionOnPiece) {
				return true
		}
	}
	return false
}

func marshalPing() string {
	ping, _ := json.Marshal(&noobDomain.Ping{MsgType: "ping"})
	return string(ping)
}

func marshalThrottle() string {
	throttleMsg, _ := json.Marshal(&noobDomain.Throttle{MsgType: "throttle", Data: throttle})
	return string(throttleMsg)
}

func marshalLaneChange(direction string) string {
	msg, _ := json.Marshal(&noobDomain.SwitchLane{ MsgType: "switchLane", Data: direction})
	return string(msg)
}
func joinMessage(botname string, botkey string) string {
	joinMessage := noobDomain.Data{ Name: botname, Key: botkey }
	joinMsg, _ := json.Marshal(&noobDomain.MessageType{ MsgType: "join", Data: joinMessage})
	return string(joinMsg)
}

func Round(x float64, prec int) float64 {
	var rounder float64
	pow := math.Pow(10, float64(prec))
	intermed := float64(x) * pow
	_, frac := math.Modf(intermed)
	if frac >= 0.5 {
		rounder = math.Ceil(intermed)
	} else {
		rounder = math.Floor(intermed)
	}
	return rounder / pow
}

func createRace(botname string, botkey string, track string, carCount int, join bool) string {
	bot := noobDomain.BotId{ Name: botname, Key: botkey }
	raceMessageData := noobDomain.CreateRaceInitData{ BotId: bot, TrackName: track, CarCount: carCount }
	msgType := "createRace"
	if(join) {
		msgType = "joinRace"
	}
	joinMsg, _ := json.Marshal(&noobDomain.CreateRaceMessageType{ MsgType: msgType, CreateRaceInitData: raceMessageData})
	fmt.Println("createRace:", string(joinMsg))
	return string(joinMsg)
}
