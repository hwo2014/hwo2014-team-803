package noobDomain

type Throttle struct {
	MsgType string 	`json:"msgType"`
	Data	float64 `json:"data,omitempty"`
}

type Ping struct {
	MsgType string 	`json:"msgType"`
}

type MessageType struct {
	MsgType string 	`json:"msgType"`
	Data	Data 	`json:"data,omitempty"`
	GameId	string 	`json:"gameId,omitempty"`
	GameTick int 	`json:"gameTick,omitempty"`
}

type CarPositions struct {
	MsgType string 	`json:"msgType"`
	CarPositionData	[]CarPositionData 	`json:"data,omitempty"`
	GameId	string 	`json:"gameId,omitempty"`
	GameTick int 	`json:"gameTick,omitempty"`
}

type CarPositionData struct {
	Name string 	`json:"name,omitempty"`
	Key	string 		`json:"key,omitempty"`
	Color	string 	`json:"color,omitempty"`
	Race Race 		`json:"race,omitempty"`
	Id Id 			`json:"id,omitempty"`
	Angle	float64	`json:"angle,omitempty"`
	PiecePosition	PiecePosition	`json:"piecePosition,omitempty"`
}

type PiecePosition struct {
	PieceIndex int			`json:"pieceIndex,omitempty"`
	InPieceDistance float32	`json:"inPieceDistance,omitempty"`
	Lane Lane				`json:"lane,omitempty"`
	Lap int					`json:"lap,omitempty"`
}

type Lane struct {
	StartLaneIndex int		`json:"startLaneIndex,omitempty"`
	EndLaneIndex int		`json:"endLaneIndex,omitempty"`
}

type Data struct {
	Name string 	`json:"name,omitempty"`
	Key	string 		`json:"key,omitempty"`
	Color	string 	`json:"color,omitempty"`
	Race Race 		`json:"race,omitempty"`
	Id []Id 		`json:"id,omitempty"`
	Car Id 			`json:"car,omitempty"`
	LapTime LapTime `json:"lapTime,omitempty"`
	RaceTime LapTime `json:"raceTime,omitempty"`
	Ranking LapTime `json:"ranking,omitempty"`
	BotId BotId		`json:"botId,omitempty"`
	TrackName string	`json:"trackName,omitempty"`
	Password string	`json:"password,omitempty"`
	CarCount int	`json:"carCount,omitempty"`
}

type Id struct {
	Name	string	`json:"name,omitempty"`
	Color 	string 	`json:"color,omitempty"`
}

type Race struct {
	Track Track 	`json:"track,omitempty"`
	RaceSession RaceSession `json:"raceSession,omitempty"`
}

type RaceSession struct {
	Laps int			`json:"laps,omitempty"`
	MaxLapTimeMs int	`json:"maxLapTimeMs,omitempty"`
	QuickRace bool		`json:"quickRace,omitempty"`
}

type Track struct {
	Id		string		`json:"id,omitempty"`
	Name	string		`json:"name,omitempty"`
	Pieces	[]Pieces 	`json:"pieces,omitempty"`
	Lanes	[]Lanes		`json:"lanes,omitempty"`
	StartingPoint StartingPoint `json:"startingPoint,omitempty"`

}

type Pieces struct {
	Length	float32		`json:"length,omitempty"`
	Switch	bool		`json:"switch,omitempty"`
	Radius	float32		`json:"radius,omitempty"`
	Angle	float64		`json:"angle,omitempty"`
}

type Lanes struct {
	DistanceFromCenter	int `json:"distanceFromCenter,omitempty"`
	Index				int `json:"index,omitempty"`
}

type StartingPoint struct {
	Position Position 	`json:"position,omitempty"`
	Angle	float64 	`json:"angle,omitempty"`
}

type Position struct {
	X float32 		`json:"x,omitempty"`
	Y float32 		`json:"y,omitempty"`
}

type LapTime struct {
	Lap int 		`json:"laps,omitempty"`
	Ticks int 		`json:"ticks,omitempty"`
	Millis int 		`json:"millis,omitempty"`
}
type RaceTime struct {
	Laps int	 	`json:"laps,omitempty"`
	Ticks int	 	`json:"ticks,omitempty"`
	Millis int 		`json:"millis,omitempty"`
}
type Ranking struct {
	Overall int 	`json:"overall,omitempty"`
	FastestLap int 	`json:"fastestLap,omitempty"`
}

type CreateRaceData struct {
	Name string 	`json:"name,omitempty"`
	Key	string 		`json:"key,omitempty"`
	Color	string 	`json:"color,omitempty"`
	Race Race 		`json:"race,omitempty"`
	Id []Id 		`json:"id,omitempty"`
	Car Id 			`json:"car,omitempty"`
	LapTime LapTime `json:"lapTime,omitempty"`
	raceTime LapTime `json:"raceTime,omitempty"`
	ranking LapTime `json:"ranking,omitempty"`
}

type CreateRaceMessageType struct {
	MsgType string 	`json:"msgType"`
	CreateRaceInitData	CreateRaceInitData 	`json:"data,omitempty"`
	GameId	string 	`json:"gameId,omitempty"`
	GameTick int 	`json:"gameTick,omitempty"`
}

type BotId struct {
	Name string		`json:"name,omitempty"`
	Key string		`json:"key,omitempty"`
}

type CreateRaceInitData struct {
	BotId BotId		`json:"botId,omitempty"`
	TrackName string	`json:"trackName,omitempty"`
	CarCount int	`json:"carCount,omitempty"`
}

type SwitchLane struct {
	MsgType	string	`json:"msgType,omitempty"`
	Data	string	`json:"data,omitempty"`
}
